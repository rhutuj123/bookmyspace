{-# LANGUAGE OverloadedStrings #-}

module HandlersSpec (handlersSpec) where

import Test.Hspec
import Database.Persist.Sql (runSqlPool)
import Database.Persist.Postgresql (withPostgresqlPool, runMigration)
import Control.Monad.Reader (runReaderT)
import Control.Monad.Logger (runStdoutLoggingT)
import Data.Time (getCurrentTime, addUTCTime)
import Models
import Handlers
import Utils (clearDB)
import Config (defaultConfig)

handlersSpec :: Spec
handlersSpec = around withDatabase $ do
    describe "addFacility" $ do
        it "should add a facility" $ \pool -> do
            let facility = Facility "Tennis Court" "Sports" 30 120
            facility' <- runSqlPool (runReaderT (addFacility facility) pool) pool
            facilityName facility' `shouldBe` "Tennis Court"

    describe "updateFacility" $ do
        it "should update a facility" $ \pool -> do
            let facility = Facility "Tennis Court" "Sports" 30 120
            facilityId <- runSqlPool (runReaderT (addFacility facility) pool) pool
            let updatedFacility = Facility "Updated Tennis Court" "Sports" 30 120
            facility' <- runSqlPool (runReaderT (updateFacility facilityId updatedFacility) pool) pool
            facilityName facility' `shouldBe` "Updated Tennis Court"

    describe "deleteFacility" $ do
        it "should delete a facility" $ \pool -> do
            let facility = Facility "Tennis Court" "Sports" 30 120
            facilityId <- runSqlPool (runReaderT (addFacility facility) pool) pool
            runSqlPool (runReaderT (deleteFacility facilityId) pool) pool
            maybeFacility <- runSqlPool (get facilityId) pool
            maybeFacility `shouldBe` Nothing

    describe "bookFacility" $ do
        it "should book a facility" $ \pool -> do
            let facility = Facility "Tennis Court" "Sports" 30 120
            facilityId <- runSqlPool (runReaderT (addFacility facility) pool) pool
            now <- getCurrentTime
            let bookingStart = now
            let bookingEnd = addUTCTime 3600 now
            let userId = 1
            booking <- runSqlPool (runReaderT (bookFacility facilityId userId bookingStart bookingEnd) pool) pool
            bookingStatus booking `shouldBe` "Pending"

    describe "cancelBooking" $ do
        it "should cancel a booking" $ \pool -> do
            let facility = Facility "Tennis Court" "Sports" 30 120
            facilityId <- runSqlPool (runReaderT (addFacility facility) pool) pool
            now <- getCurrentTime
            let bookingStart = now
            let bookingEnd = addUTCTime 3600 now
            let userId = 1
            bookingId <- runSqlPool (runReaderT (bookFacility facilityId userId bookingStart bookingEnd) pool) pool
            runSqlPool (runReaderT (cancelBooking bookingId "User requested") pool) pool
            maybeBooking <- runSqlPool (get bookingId) pool
            case maybeBooking of
                Just booking -> bookingStatus booking `shouldBe` "Cancelled"
                Nothing -> expectationFailure "Booking not found"

