{-# LANGUAGE OverloadedStrings #-}

module Utils where

import Control.Monad.Logger (runStdoutLoggingT)
import Database.Persist.Postgresql (withPostgresqlPool, runSqlPool, rawExecute)
import Config (defaultConfig)

withDatabase :: (ConnectionPool -> IO ()) -> IO ()
withDatabase action = runStdoutLoggingT $ withPostgresqlPool (getConnStr defaultConfig) 10 $ \pool -> do
    liftIO $ runSqlPool (rawExecute "TRUNCATE facility, \"group\", facility_group, booking, holiday, rating RESTART IDENTITY" []) pool
    liftIO $ action pool

clearDB :: ConnectionPool -> IO ()
clearDB pool = runSqlPool (rawExecute "TRUNCATE facility, \"group\", facility_group, booking, holiday, rating RESTART IDENTITY" []) pool
