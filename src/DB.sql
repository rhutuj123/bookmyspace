CREATE TABLE facility (
    id SERIAL PRIMARY KEY,
    name TEXT NOT NULL,
    facility_type TEXT NOT NULL,
    min_booking_duration INT NOT NULL,
    max_booking_duration INT NOT NULL
);

CREATE TABLE "group" (
    id SERIAL PRIMARY KEY,
    name TEXT NOT NULL
);

CREATE TABLE facility_group (
    id SERIAL PRIMARY KEY,
    facility_id INT REFERENCES facility(id) ON DELETE CASCADE,
    group_id INT REFERENCES "group"(id) ON DELETE CASCADE
);

CREATE TABLE booking (
    id SERIAL PRIMARY KEY,
    facility_id INT REFERENCES facility(id) ON DELETE CASCADE,
    user_id INT NOT NULL,
    start_time TIMESTAMP NOT NULL,
    end_time TIMESTAMP NOT NULL,
    status TEXT NOT NULL
);

CREATE TABLE holiday (
    id SERIAL PRIMARY KEY,
    facility_id INT REFERENCES facility(id) ON DELETE CASCADE,
    group_id INT REFERENCES "group"(id) ON DELETE CASCADE,
    start_time TIMESTAMP NOT NULL,
    end_time TIMESTAMP NOT NULL
);

CREATE TABLE rating (
    id SERIAL PRIMARY KEY,
    facility_id INT REFERENCES facility(id) ON DELETE CASCADE,
    rating INT NOT NULL,
    comment TEXT
);
