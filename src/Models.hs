{-# LANGUAGE GADTs #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Models where

import Data.Text (Text)
import Database.Persist.TH
import Data.Time (UTCTime)
import GHC.Generics (Generic)
import Data.Aeson (FromJSON, ToJSON)

share [mkPersist sqlSettings, mkMigrate "migrateAll"] [persistLowerCase|
Facility
    name Text
    facilityType Text
    minBookingDuration Int
    maxBookingDuration Int
    deriving Show Generic
Group
    name Text
    deriving Show Generic
FacilityGroup
    facilityId FacilityId
    groupId GroupId
    deriving Show Generic
Booking
    facilityId FacilityId
    userId Int
    startTime UTCTime
    endTime UTCTime
    status Text
    deriving Show Generic
Holiday
    facilityId FacilityId Maybe
    groupId GroupId Maybe
    startTime UTCTime
    endTime UTCTime
    deriving Show Generic
Rating
    facilityId FacilityId
    rating Int
    comment Text
    deriving Show Generic
|]

instance ToJSON Facility
instance FromJSON Facility
instance ToJSON Group
instance FromJSON Group
instance ToJSON FacilityGroup
instance FromJSON FacilityGroup
instance ToJSON Booking
instance FromJSON Booking
instance ToJSON Holiday
instance FromJSON Holiday
instance ToJSON Rating
instance FromJSON Rating
