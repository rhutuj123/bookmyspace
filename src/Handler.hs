{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}

module Handlers where

import Servant
import Database.Persist
import Database.Persist.Sql
import Models
import Control.Monad.IO.Class (liftIO)
import Data.Time.Clock (UTCTime, getCurrentTime, addUTCTime)
import Types

type AppM = ReaderT SqlBackend Handler

addFacility :: Facility -> AppM Facility
addFacility facility = do
    facilityId <- insert facility
    return facility

updateFacility :: FacilityId -> Facility -> AppM Facility
updateFacility fid newFacility = do
    replace fid newFacility
    return newFacility

deleteFacility :: FacilityId -> AppM ()
deleteFacility fid = delete fid

createGroup :: Group -> AppM Group
createGroup group = do
    groupId <- insert group
    return group

addFacilityToGroup :: FacilityId -> GroupId -> AppM ()
addFacilityToGroup fid gid = do
    _ <- insert $ FacilityGroup fid gid
    return ()

removeFacilityFromGroup :: FacilityId -> GroupId -> AppM ()
removeFacilityFromGroup fid gid = do
    deleteWhere [FacilityGroupFacilityId ==. fid, FacilityGroupGroupId ==. gid]
    return ()

setHoliday :: Maybe FacilityId -> Maybe GroupId -> UTCTime -> UTCTime -> AppM Holiday
setHoliday mFid mGid start end = do
    let holiday = Holiday mFid mGid start end
    holidayId <- insert holiday
    return holiday

removeHoliday :: HolidayId -> AppM ()
removeHoliday hid = delete hid

getFacilities :: AppM [Entity Facility]
getFacilities = selectList [] []

getFacility :: FacilityId -> AppM (Maybe Facility)
getFacility fid = get fid

bookFacility :: FacilityId -> Int -> UTCTime -> UTCTime -> AppM Booking
bookFacility fid userId start end = do
    let booking = Booking fid userId start end "Pending"
    bookingId <- insert booking
    return booking

cancelBooking :: BookingId -> Text -> AppM ()
cancelBooking bid reason = do
    update bid [BookingStatus =. "Cancelled"]
    return ()

getBookings :: AppM [Entity Booking]
getBookings = selectList [] []

getBooking :: BookingId -> AppM (Maybe Booking)
getBooking bid = get bid

activateBooking :: BookingId -> Text -> AppM ()
activateBooking bid otp = do
    update bid [BookingStatus =. "Active"]
    return ()

searchFacilities :: Maybe Text -> Maybe (UTCTime, UTCTime) -> AppM [Entity Facility]
searchFacilities mType mRange = do
    facilities <- selectList [] []
    return facilities

searchFacilitiesByTime :: (UTCTime, UTCTime) -> AppM [Entity Facility]
searchFacilitiesByTime (start, end) = do
    facilities <- selectList [] []
    return facilities

addFacilityRating :: FacilityId -> Int -> Text -> AppM ()
addFacilityRating fid rating comment = do
    _ <- insert $ Rating fid rating comment
    return ()

getFacilityRatings :: FacilityId -> AppM [Entity Rating]
getFacilityRatings fid = do
    ratings <- selectList [RatingFacilityId ==. fid] []
    return ratings

exportFacilityData :: FacilityId -> Text -> AppM FilePath
exportFacilityData fid fileType = do
    -- Export logic here
    return "path/to/exported/file"
