module Api where

import Servant
import Handlers (addFacility, updateFacility, deleteFacility, createGroup, addFacilityToGroup, removeFacilityFromGroup, deleteGroup, setHolidayForFacility, setHolidayForGroup, removeHoliday, getFacilities, getFacility, bookFacility, cancelBooking, getUserBookings, getBooking, getBookingStatus, activateBooking, searchFacility, searchFacilityByTime, addFacilityRating, getFacilityRatings, exportFacilityData)
import Types

type API = "admin" :> "add_facility" :> ReqBody '[JSON] Facility :> Post '[JSON] Facility
      :<|> "admin" :> "update_facility" :> Capture "facility_id" FacilityId :> ReqBody '[JSON] Facility :> Put '[JSON] Facility
      :<|> "admin" :> "delete_facility" :> Capture "facility_id" FacilityId :> Delete '[JSON] NoContent
      

server :: Server API
server = addFacility
    :<|> updateFacility
    :<|> deleteFacility
    

app :: Application
app = serve (Proxy :: Proxy API) server
