/* DO NOT EDIT: This file is automatically generated by Cabal */

/* package bookmyspace-0.1.0.0 */
#ifndef VERSION_bookmyspace
#define VERSION_bookmyspace "0.1.0.0"
#endif /* VERSION_bookmyspace */
#ifndef MIN_VERSION_bookmyspace
#define MIN_VERSION_bookmyspace(major1,major2,minor) (\
  (major1) <  0 || \
  (major1) == 0 && (major2) <  1 || \
  (major1) == 0 && (major2) == 1 && (minor) <= 0)
#endif /* MIN_VERSION_bookmyspace */
/* package aeson-2.2.3.0 */
#ifndef VERSION_aeson
#define VERSION_aeson "2.2.3.0"
#endif /* VERSION_aeson */
#ifndef MIN_VERSION_aeson
#define MIN_VERSION_aeson(major1,major2,minor) (\
  (major1) <  2 || \
  (major1) == 2 && (major2) <  2 || \
  (major1) == 2 && (major2) == 2 && (minor) <= 3)
#endif /* MIN_VERSION_aeson */
/* package base-4.17.2.1 */
#ifndef VERSION_base
#define VERSION_base "4.17.2.1"
#endif /* VERSION_base */
#ifndef MIN_VERSION_base
#define MIN_VERSION_base(major1,major2,minor) (\
  (major1) <  4 || \
  (major1) == 4 && (major2) <  17 || \
  (major1) == 4 && (major2) == 17 && (minor) <= 2)
#endif /* MIN_VERSION_base */
/* package bytestring-0.11.5.3 */
#ifndef VERSION_bytestring
#define VERSION_bytestring "0.11.5.3"
#endif /* VERSION_bytestring */
#ifndef MIN_VERSION_bytestring
#define MIN_VERSION_bytestring(major1,major2,minor) (\
  (major1) <  0 || \
  (major1) == 0 && (major2) <  11 || \
  (major1) == 0 && (major2) == 11 && (minor) <= 5)
#endif /* MIN_VERSION_bytestring */
/* package containers-0.6.7 */
#ifndef VERSION_containers
#define VERSION_containers "0.6.7"
#endif /* VERSION_containers */
#ifndef MIN_VERSION_containers
#define MIN_VERSION_containers(major1,major2,minor) (\
  (major1) <  0 || \
  (major1) == 0 && (major2) <  6 || \
  (major1) == 0 && (major2) == 6 && (minor) <= 7)
#endif /* MIN_VERSION_containers */
/* package mtl-2.2.2 */
#ifndef VERSION_mtl
#define VERSION_mtl "2.2.2"
#endif /* VERSION_mtl */
#ifndef MIN_VERSION_mtl
#define MIN_VERSION_mtl(major1,major2,minor) (\
  (major1) <  2 || \
  (major1) == 2 && (major2) <  2 || \
  (major1) == 2 && (major2) == 2 && (minor) <= 2)
#endif /* MIN_VERSION_mtl */
/* package persistent-2.14.6.1 */
#ifndef VERSION_persistent
#define VERSION_persistent "2.14.6.1"
#endif /* VERSION_persistent */
#ifndef MIN_VERSION_persistent
#define MIN_VERSION_persistent(major1,major2,minor) (\
  (major1) <  2 || \
  (major1) == 2 && (major2) <  14 || \
  (major1) == 2 && (major2) == 14 && (minor) <= 6)
#endif /* MIN_VERSION_persistent */
/* package persistent-sqlite-2.13.3.0 */
#ifndef VERSION_persistent_sqlite
#define VERSION_persistent_sqlite "2.13.3.0"
#endif /* VERSION_persistent_sqlite */
#ifndef MIN_VERSION_persistent_sqlite
#define MIN_VERSION_persistent_sqlite(major1,major2,minor) (\
  (major1) <  2 || \
  (major1) == 2 && (major2) <  13 || \
  (major1) == 2 && (major2) == 13 && (minor) <= 3)
#endif /* MIN_VERSION_persistent_sqlite */
/* package persistent-template-2.12.0.0 */
#ifndef VERSION_persistent_template
#define VERSION_persistent_template "2.12.0.0"
#endif /* VERSION_persistent_template */
#ifndef MIN_VERSION_persistent_template
#define MIN_VERSION_persistent_template(major1,major2,minor) (\
  (major1) <  2 || \
  (major1) == 2 && (major2) <  12 || \
  (major1) == 2 && (major2) == 12 && (minor) <= 0)
#endif /* MIN_VERSION_persistent_template */
/* package random-1.2.1.2 */
#ifndef VERSION_random
#define VERSION_random "1.2.1.2"
#endif /* VERSION_random */
#ifndef MIN_VERSION_random
#define MIN_VERSION_random(major1,major2,minor) (\
  (major1) <  1 || \
  (major1) == 1 && (major2) <  2 || \
  (major1) == 1 && (major2) == 2 && (minor) <= 1)
#endif /* MIN_VERSION_random */
/* package servant-0.20.1 */
#ifndef VERSION_servant
#define VERSION_servant "0.20.1"
#endif /* VERSION_servant */
#ifndef MIN_VERSION_servant
#define MIN_VERSION_servant(major1,major2,minor) (\
  (major1) <  0 || \
  (major1) == 0 && (major2) <  20 || \
  (major1) == 0 && (major2) == 20 && (minor) <= 1)
#endif /* MIN_VERSION_servant */
/* package servant-auth-server-0.4.8.0 */
#ifndef VERSION_servant_auth_server
#define VERSION_servant_auth_server "0.4.8.0"
#endif /* VERSION_servant_auth_server */
#ifndef MIN_VERSION_servant_auth_server
#define MIN_VERSION_servant_auth_server(major1,major2,minor) (\
  (major1) <  0 || \
  (major1) == 0 && (major2) <  4 || \
  (major1) == 0 && (major2) == 4 && (minor) <= 8)
#endif /* MIN_VERSION_servant_auth_server */
/* package servant-client-0.20 */
#ifndef VERSION_servant_client
#define VERSION_servant_client "0.20"
#endif /* VERSION_servant_client */
#ifndef MIN_VERSION_servant_client
#define MIN_VERSION_servant_client(major1,major2,minor) (\
  (major1) <  0 || \
  (major1) == 0 && (major2) <  20 || \
  (major1) == 0 && (major2) == 20 && (minor) <= 0)
#endif /* MIN_VERSION_servant_client */
/* package servant-server-0.20 */
#ifndef VERSION_servant_server
#define VERSION_servant_server "0.20"
#endif /* VERSION_servant_server */
#ifndef MIN_VERSION_servant_server
#define MIN_VERSION_servant_server(major1,major2,minor) (\
  (major1) <  0 || \
  (major1) == 0 && (major2) <  20 || \
  (major1) == 0 && (major2) == 20 && (minor) <= 0)
#endif /* MIN_VERSION_servant_server */
/* package servant-swagger-1.2 */
#ifndef VERSION_servant_swagger
#define VERSION_servant_swagger "1.2"
#endif /* VERSION_servant_swagger */
#ifndef MIN_VERSION_servant_swagger
#define MIN_VERSION_servant_swagger(major1,major2,minor) (\
  (major1) <  1 || \
  (major1) == 1 && (major2) <  2 || \
  (major1) == 1 && (major2) == 2 && (minor) <= 0)
#endif /* MIN_VERSION_servant_swagger */
/* package text-2.0.2 */
#ifndef VERSION_text
#define VERSION_text "2.0.2"
#endif /* VERSION_text */
#ifndef MIN_VERSION_text
#define MIN_VERSION_text(major1,major2,minor) (\
  (major1) <  2 || \
  (major1) == 2 && (major2) <  0 || \
  (major1) == 2 && (major2) == 0 && (minor) <= 2)
#endif /* MIN_VERSION_text */
/* package time-1.12.2 */
#ifndef VERSION_time
#define VERSION_time "1.12.2"
#endif /* VERSION_time */
#ifndef MIN_VERSION_time
#define MIN_VERSION_time(major1,major2,minor) (\
  (major1) <  1 || \
  (major1) == 1 && (major2) <  12 || \
  (major1) == 1 && (major2) == 12 && (minor) <= 2)
#endif /* MIN_VERSION_time */
/* package transformers-0.5.6.2 */
#ifndef VERSION_transformers
#define VERSION_transformers "0.5.6.2"
#endif /* VERSION_transformers */
#ifndef MIN_VERSION_transformers
#define MIN_VERSION_transformers(major1,major2,minor) (\
  (major1) <  0 || \
  (major1) == 0 && (major2) <  5 || \
  (major1) == 0 && (major2) == 5 && (minor) <= 6)
#endif /* MIN_VERSION_transformers */
/* package wai-3.2.4 */
#ifndef VERSION_wai
#define VERSION_wai "3.2.4"
#endif /* VERSION_wai */
#ifndef MIN_VERSION_wai
#define MIN_VERSION_wai(major1,major2,minor) (\
  (major1) <  3 || \
  (major1) == 3 && (major2) <  2 || \
  (major1) == 3 && (major2) == 2 && (minor) <= 4)
#endif /* MIN_VERSION_wai */
/* package wai-extra-3.1.15 */
#ifndef VERSION_wai_extra
#define VERSION_wai_extra "3.1.15"
#endif /* VERSION_wai_extra */
#ifndef MIN_VERSION_wai_extra
#define MIN_VERSION_wai_extra(major1,major2,minor) (\
  (major1) <  3 || \
  (major1) == 3 && (major2) <  1 || \
  (major1) == 3 && (major2) == 1 && (minor) <= 15)
#endif /* MIN_VERSION_wai_extra */
/* package warp-3.4.1 */
#ifndef VERSION_warp
#define VERSION_warp "3.4.1"
#endif /* VERSION_warp */
#ifndef MIN_VERSION_warp
#define MIN_VERSION_warp(major1,major2,minor) (\
  (major1) <  3 || \
  (major1) == 3 && (major2) <  4 || \
  (major1) == 3 && (major2) == 4 && (minor) <= 1)
#endif /* MIN_VERSION_warp */
/* package yaml-0.11.11.2 */
#ifndef VERSION_yaml
#define VERSION_yaml "0.11.11.2"
#endif /* VERSION_yaml */
#ifndef MIN_VERSION_yaml
#define MIN_VERSION_yaml(major1,major2,minor) (\
  (major1) <  0 || \
  (major1) == 0 && (major2) <  11 || \
  (major1) == 0 && (major2) == 11 && (minor) <= 11)
#endif /* MIN_VERSION_yaml */

/* tool gcc-11 */
#ifndef TOOL_VERSION_gcc
#define TOOL_VERSION_gcc "11"
#endif /* TOOL_VERSION_gcc */
#ifndef MIN_TOOL_VERSION_gcc
#define MIN_TOOL_VERSION_gcc(major1,major2,minor) (\
  (major1) <  11 || \
  (major1) == 11 && (major2) <  0 || \
  (major1) == 11 && (major2) == 0 && (minor) <= 0)
#endif /* MIN_TOOL_VERSION_gcc */
/* tool ghc-9.4.8 */
#ifndef TOOL_VERSION_ghc
#define TOOL_VERSION_ghc "9.4.8"
#endif /* TOOL_VERSION_ghc */
#ifndef MIN_TOOL_VERSION_ghc
#define MIN_TOOL_VERSION_ghc(major1,major2,minor) (\
  (major1) <  9 || \
  (major1) == 9 && (major2) <  4 || \
  (major1) == 9 && (major2) == 4 && (minor) <= 8)
#endif /* MIN_TOOL_VERSION_ghc */
/* tool ghc-pkg-9.4.8 */
#ifndef TOOL_VERSION_ghc_pkg
#define TOOL_VERSION_ghc_pkg "9.4.8"
#endif /* TOOL_VERSION_ghc_pkg */
#ifndef MIN_TOOL_VERSION_ghc_pkg
#define MIN_TOOL_VERSION_ghc_pkg(major1,major2,minor) (\
  (major1) <  9 || \
  (major1) == 9 && (major2) <  4 || \
  (major1) == 9 && (major2) == 4 && (minor) <= 8)
#endif /* MIN_TOOL_VERSION_ghc_pkg */
/* tool haddock-2.27.0 */
#ifndef TOOL_VERSION_haddock
#define TOOL_VERSION_haddock "2.27.0"
#endif /* TOOL_VERSION_haddock */
#ifndef MIN_TOOL_VERSION_haddock
#define MIN_TOOL_VERSION_haddock(major1,major2,minor) (\
  (major1) <  2 || \
  (major1) == 2 && (major2) <  27 || \
  (major1) == 2 && (major2) == 27 && (minor) <= 0)
#endif /* MIN_TOOL_VERSION_haddock */
/* tool hpc-0.68 */
#ifndef TOOL_VERSION_hpc
#define TOOL_VERSION_hpc "0.68"
#endif /* TOOL_VERSION_hpc */
#ifndef MIN_TOOL_VERSION_hpc
#define MIN_TOOL_VERSION_hpc(major1,major2,minor) (\
  (major1) <  0 || \
  (major1) == 0 && (major2) <  68 || \
  (major1) == 0 && (major2) == 68 && (minor) <= 0)
#endif /* MIN_TOOL_VERSION_hpc */
/* tool hsc2hs-0.68.8 */
#ifndef TOOL_VERSION_hsc2hs
#define TOOL_VERSION_hsc2hs "0.68.8"
#endif /* TOOL_VERSION_hsc2hs */
#ifndef MIN_TOOL_VERSION_hsc2hs
#define MIN_TOOL_VERSION_hsc2hs(major1,major2,minor) (\
  (major1) <  0 || \
  (major1) == 0 && (major2) <  68 || \
  (major1) == 0 && (major2) == 68 && (minor) <= 8)
#endif /* MIN_TOOL_VERSION_hsc2hs */
/* tool runghc-9.4.8 */
#ifndef TOOL_VERSION_runghc
#define TOOL_VERSION_runghc "9.4.8"
#endif /* TOOL_VERSION_runghc */
#ifndef MIN_TOOL_VERSION_runghc
#define MIN_TOOL_VERSION_runghc(major1,major2,minor) (\
  (major1) <  9 || \
  (major1) == 9 && (major2) <  4 || \
  (major1) == 9 && (major2) == 4 && (minor) <= 8)
#endif /* MIN_TOOL_VERSION_runghc */
/* tool strip-2.38 */
#ifndef TOOL_VERSION_strip
#define TOOL_VERSION_strip "2.38"
#endif /* TOOL_VERSION_strip */
#ifndef MIN_TOOL_VERSION_strip
#define MIN_TOOL_VERSION_strip(major1,major2,minor) (\
  (major1) <  2 || \
  (major1) == 2 && (major2) <  38 || \
  (major1) == 2 && (major2) == 38 && (minor) <= 0)
#endif /* MIN_TOOL_VERSION_strip */

#ifndef CURRENT_PACKAGE_KEY
#define CURRENT_PACKAGE_KEY "bookmyspace-0.1.0.0-inplace"
#endif /* CURRENT_packageKey */
#ifndef CURRENT_COMPONENT_ID
#define CURRENT_COMPONENT_ID "bookmyspace-0.1.0.0-inplace"
#endif /* CURRENT_COMPONENT_ID */
#ifndef CURRENT_PACKAGE_VERSION
#define CURRENT_PACKAGE_VERSION "0.1.0.0"
#endif /* CURRENT_PACKAGE_VERSION */
